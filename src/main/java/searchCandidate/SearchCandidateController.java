package searchCandidate;

import java.net.URL;
import java.sql.ResultSet;
import java.util.ResourceBundle;

import addCandidate.AddCandidate;
import candidates.Candidates;
import dashboard.Dashboard;
import dbOperation.DbUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import recrutement.Recrutement;
import searchUser.Admin;
import users.User;

public class SearchCandidateController implements Initializable{
	@FXML
	private Button search;
	@FXML
	private Button admin;
	@FXML
	private Button recruitment;
	@FXML
	private Button dashboard;
	@FXML
	private TextField candid;
	@FXML
	private TableView<Candidate> tableView;
	@FXML
	private TableColumn<Candidate,String> col1;
	@FXML
	private TableColumn<Candidate,String> col2;
	@FXML
	private TableColumn<Candidate,String> col3;
	@FXML
	private TableColumn<Candidate,String> col4;
	@FXML
	private TableColumn<Candidate,String> col5;
	@FXML
	private TableColumn<Candidate,String> col6;
	@FXML
	private TableColumn<Candidate,String> col7;
	@FXML
	private Button back;

	public void back(ActionEvent event) {
		new Candidates().show();
	}
	private ObservableList<Candidate> data;

	public void search(ActionEvent event) {
		new Dashboard().show();
		
	}
	public void admin(ActionEvent event) {
		new  User().show();
	}
	public void recrutement(ActionEvent event) {
		new Recrutement().show();
	}
	public void dashboard(ActionEvent event ) {
		new Dashboard().show();
	}

	@Override
	public void initialize(URL url, ResourceBundle rb) {

		data=FXCollections.observableArrayList();

		
		col1.setCellValueFactory(new PropertyValueFactory<Candidate, String>("firstName"));
		col2.setCellValueFactory(new PropertyValueFactory<Candidate, String>("lastName"));
		col3.setCellValueFactory(new PropertyValueFactory<Candidate, String>("vacancy"));
		col4.setCellValueFactory(new PropertyValueFactory<Candidate, String>("email"));
		col5.setCellValueFactory(new PropertyValueFactory<Candidate, String>("contactNumber"));
		col6.setCellValueFactory(new PropertyValueFactory<Candidate, String>("keyword"));
		col7.setCellValueFactory(new PropertyValueFactory<Candidate, String>("note"));
	buildData();
		
		FilteredList<Candidate> filteredData=new FilteredList<>(data,b->true);
		candid.textProperty().addListener((observable,oldValue,newValue)->{
		filteredData.setPredicate(Candidate->{
			if(newValue.isEmpty() || newValue.isBlank() || newValue==null ) {
				return true;
			}
			String searchKeyword=newValue.toLowerCase();
			if(Candidate.getFirstName().toLowerCase().indexOf(searchKeyword)> -1) {
				return true;
			}else if(Candidate.getLastName().toLowerCase().indexOf(searchKeyword)> -1) {
				return true;
			}else if(Candidate.getVacancy().toLowerCase().indexOf(searchKeyword)> -1) {
				return true;
			}else if(Candidate.getEmail().toLowerCase().indexOf(searchKeyword)> -1) {
				return true;
			}else if(Candidate.getContactNumber().toLowerCase().indexOf(searchKeyword)> -1) {
				return true;
			}else if(Candidate.getKeyword().toLowerCase().indexOf(searchKeyword)> -1) {
				return true;
			}else if(Candidate.getNote().toLowerCase().indexOf(searchKeyword)> -1) {
				return true;
			}else  {
				return false;
			}
				
			
		});
		});
		

		
		SortedList<Candidate> sortedData =new SortedList<>(filteredData);

		sortedData.comparatorProperty().bind(tableView.comparatorProperty());
		tableView.setItems(sortedData);
		
		
	}
	public void buildData()
	{
		try {
			data=FXCollections.observableArrayList();
			String query = "Select*from candidate";
			System.out.println(query);
			ResultSet resultSet = DbUtil.executeQueryGetResult(query);
			while (resultSet.next()) {
				Candidate ca=new Candidate();
				ca.firstName.set(resultSet.getString(1));
				ca.lastName.set(resultSet.getString(2));
				ca.vacancy.set(resultSet.getString(3));
				ca.email.set(resultSet.getString(4));
				ca.contactNumber.set(resultSet.getString(5));

				ca.keyword.set(resultSet.getString(6));


				ca.note.set(resultSet.getString(7));

				data.add(ca);
			}
			tableView.setItems(data);
			
		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}

}
