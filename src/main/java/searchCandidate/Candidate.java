package searchCandidate;

import javafx.beans.property.SimpleStringProperty;

public class Candidate {
	public SimpleStringProperty firstName=new SimpleStringProperty();

	public SimpleStringProperty lastName=new SimpleStringProperty();

	public SimpleStringProperty vacancy=new SimpleStringProperty();
	public SimpleStringProperty email=new SimpleStringProperty();
	public SimpleStringProperty contactNumber=new SimpleStringProperty();
	public SimpleStringProperty keyword=new SimpleStringProperty();
	public SimpleStringProperty dateOfApplication=new SimpleStringProperty();
	public SimpleStringProperty note=new SimpleStringProperty();
	public SimpleStringProperty date=new SimpleStringProperty();


	  public String getFirstName(){
	       return firstName.get();
	   }

	   

	   public String getLastName(){
	       return lastName.get();
	   }

	   public String getVacancy(){
	       return vacancy.get();
	   }

	   public String getEmail(){
	       return email.get();
	   } public String getContactNumber(){
	       return contactNumber.get();
	   }
	   public String getKeyword(){
	       return keyword.get();
	   }
	   
	   public String getNote(){
	       return note.get();
	   }
	  
	   
	   
	   
	   

}