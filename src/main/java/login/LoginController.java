package login;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.cj.xdevapi.Statement;

import dashboard.Dashboard;
import dbOperation.DbUtil;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class LoginController {
	

	static Statement stmt;
	static Connection con;
	@FXML
	private TextField userName;
	@FXML
	private TextField password;
	@FXML
	private Button login;
	 // reference to the DialogPane created in FXML
  
	
	
	public void login(ActionEvent event) throws SQLException {

	
		String query = "Select count(*) from Users where userName='"+userName.getText()+"' and Password='"+password.getText()+"';";
		System.out.println(query);
		ResultSet resultSet = DbUtil.executeQueryGetResult(query);
		resultSet.next(); //cursor will come to next value
		if(resultSet.getInt(1)==1)
		{
			new Dashboard().show();
		
		}
		else {
			// show the alert dialog
			Alert alert = new Alert(AlertType.WARNING);
				alert.setTitle("An error has occurred");
				alert.setHeaderText("Warning !!!!!");
				alert.setContentText("Please check your input and try again.\n"
						+ "You entered wrong combination of user name or password ");
			    //alert.getDialogPane().setContent(errorDialog);
			alert.showAndWait();
			}

	}
}
