package recrutement;

import candidates.Candidates;
import dashboard.Dashboard;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import users.User;
import vacancies.Vacancies;

public class RecrutementController {
	@FXML
	private Button search;
	@FXML
	private Button admin;
	@FXML
	private Button recruitment;
	@FXML
	private Button dashboard;
	
	@FXML
	private Button vacancies;
	@FXML
	private Button candidates;
	@FXML
	private Button back;

	public void back(ActionEvent event) {
		new Dashboard().show();
	}
	public void search(ActionEvent event) {
		new Dashboard().show();
		
	}
	public void admin(ActionEvent event) {
		new  User().show();
	}
	public void recruitment(ActionEvent event) {
		new Recrutement().show();
	}
	public void dashboard(ActionEvent event ) {
		new Dashboard().show();
	}
	public void candidates(ActionEvent event) {
		new Candidates().show();
	}
	public void vacancies(ActionEvent event ) {
		new Vacancies().show();
	}

}
