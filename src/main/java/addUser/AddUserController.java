package addUser;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import dashboard.Dashboard;
import dbOperation.DbUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import recrutement.Recrutement;
import searchUser.SearchUser;
import searchVacancy.SearchVacancy;
import users.User;
import javax.swing.JOptionPane;

public class AddUserController implements Initializable {
	@FXML
	private ComboBox userRole;
	@FXML
	private ComboBox status;
	@FXML
	private TextField employeeName;
	@FXML
	private TextField userName;
	@FXML
	private TextField password;
	@FXML
	private TextField confirmPassword;
	@FXML
	private Button save;
	@FXML
	private Button search;
	@FXML
	private Button back;
	@FXML
	private Button admin;
	@FXML
	private Button recruitment;
	@FXML
	private Button dashboard;

	@FXML
	void Select(ActionEvent event) {
		String s = userRole.getSelectionModel().getSelectedItem().toString();

	}

	@Override
	public void initialize(URL url, ResourceBundle rb) {
		ObservableList<String> list = FXCollections.observableArrayList("Admin", "ESS");
		userRole.setItems(list);
		ObservableList<String> list2 = FXCollections.observableArrayList("Enable", "Disable");
		status.setItems(list2);
	}

	public void admin(ActionEvent event) {

		new User().show();

	}

	public void search(ActionEvent event) {

	}

	public void recruitment(ActionEvent event) {
		new Recrutement().show();
	}

	public void dashboard(ActionEvent event) {
		new Dashboard().show();

	}

	public void back(ActionEvent event) {
		new User().show();

	}

	public void save(ActionEvent event) throws SQLException {

		if (password.getText().equals(confirmPassword.getText())) {
			
			
			//new SearchUser().show();
			  if (employeeName.getText().isEmpty() || userName.getText().isEmpty()  || confirmPassword.getText().isEmpty() || password.getText().isEmpty() ) {
	              Alert alert = new Alert(Alert.AlertType.ERROR);
	              alert.setTitle("Error");
	              alert.setHeaderText(null);
	              alert.setContentText("Please fill in all fields.");
	              alert.showAndWait();
	              return;
			  }
	    		  String query1="select count(*) from users where userName='" +userName.getText() + "';";

	    		  ResultSet rs1=DbUtil.executeQueryGetResult(query1);
	    		  rs1.next();
	    		  if(rs1.getInt(1)==1) {
	    			  System.out.println("this userName is already present");
	    			  Alert alert1 = new Alert(Alert.AlertType.ERROR);
	                  alert1.setTitle("Error");
	                  alert1.setHeaderText(null);
	                  alert1.setContentText("Please change the userName beacause this userName is already exist");
	                  alert1.showAndWait();
	                  return;

	    		  }else {
	          
			  String query = "insert into users(userName,password,User_Role,status,Employee_Name,Confirm_Password) values ('"
						+ userName.getText() + "','" + password.getText() + "','" + userRole.getValue() + "', '"
						+ status.getValue() + "','" + employeeName.getText() + "','" + confirmPassword.getText() + "');";
				System.out.println(query);
				System.out.println(userRole.getValue());
				System.out.println(status.getValue());
				System.out.println(employeeName.getText());
				System.out.println(userName.getText());
				System.out.println(password.getText());
				System.out.println(confirmPassword.getText());
				System.out.println("Event occur admin controller " + event.getEventType().getName());
				DbUtil.executeQuery(query);
				new SearchUser().show();
				
	    		  }
		} else {
			System.out.println(" Failed");
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("Fill correct userName and password");
			alert.showAndWait();
			return;
		}

	}
}