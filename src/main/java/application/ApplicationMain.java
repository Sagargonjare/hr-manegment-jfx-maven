package application;

import addUser.AddUser;
import dashboard.Dashboard;
import dbOperation.DbUtil;
import editCandidate.EditCandidate;
import editVacancy.EditVacancy;
import javafx.application.Application;
import javafx.stage.Stage;
import login.Login;
import stageMaster.StageMaster;

public class ApplicationMain extends Application{
	public static void main(String args[]) {
		DbUtil.createDbConnection();
		launch(args);
		
	}
	public void start(Stage primaryStage) {
		StageMaster.setStage(primaryStage);
		new EditCandidate().show();
		
	}

}

