package deleteUser;

import java.sql.ResultSet;
import java.sql.SQLException;

import candidates.Candidates;
import dashboard.Dashboard;
import dbOperation.DbUtil;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import recrutement.Recrutement;
import searchUser.SearchUser;
import users.User;
import vacancies.Vacancies;

public class DeleteUserController {
	@FXML
	private Button delete;
	@FXML
	private Button search;
	
	@FXML
	private Button admin;
	@FXML
	private Button recruitment;
	@FXML
	private TextField name;
	@FXML
	private Button back;

	public void back(ActionEvent event) {
		new User().show();
	}
	public void admin(ActionEvent event) {

		new User().show();

	}

	public void search(ActionEvent event) {
			new Dashboard().show();
	}

	public void recruitment(ActionEvent event) {
			new Recrutement().show();
	}

	public void dashboard(ActionEvent event) {
		new Dashboard().show();

	}

	public void delete(ActionEvent event) throws SQLException {
		System.out.println(name.getText());
		String query1="select count(*) from users where userName='" +name.getText() + "';";

		  ResultSet rs1=DbUtil.executeQueryGetResult(query1);
		  rs1.next();
		  if(rs1.getInt(1)==1) {
		String query = " delete from users where userName ='" + name.getText() + "';";
		System.out.println(query);
		DbUtil.executeQuery(query);
		System.out.println(name.getText());

		System.out.println("Event occur delete controller " + event.getEventType().getName());
		new SearchUser().show();
		  }else {
			 
					System.out.println(" Failed");
					Alert alert = new Alert(Alert.AlertType.ERROR);
					alert.setTitle("Error");
					alert.setHeaderText(null);
					alert.setContentText("Ensure the username is already present");
					alert.showAndWait();
					return;
		  }
	}
}
