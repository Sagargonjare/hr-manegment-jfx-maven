package vacancies;

import addVacancy.AddVacancy;
import candidates.Candidates;
import dashboard.Dashboard;
import deleteVacancy.DeleteVacancy;
import editVacancy.EditVacancy;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import recrutement.Recrutement;
import searchVacancy.SearchVacancy;
import users.User;

public class VacanciesController {
	@FXML
	private Button search;
	@FXML
	private Button dashboard;
	@FXML
	private Button recrutement;
	@FXML
	private Button admin;
	@FXML
	private Button back;

	public void back(ActionEvent event) {
		new Vacancies().show();
	}

	public void edit(ActionEvent event) {
		new EditVacancy().show();
	}
	
	
	public void search(ActionEvent event) {
		new Dashboard().show();
		
	}
	public void admin(ActionEvent event) {
		new  User().show();
	}
	public void recrutement(ActionEvent event) {
		new Recrutement().show();
	}
	public void dashboard(ActionEvent event ) {
		new Dashboard().show();
	}
	public void candidates(ActionEvent event) {
		new Candidates().show();
	}
	public void add(ActionEvent event ) {
		new AddVacancy().show();
	}
	public void delete(ActionEvent event ) {
		new DeleteVacancy().show();
		
	}public void searchv(ActionEvent event ) {
		new SearchVacancy().show();
	}


}
