package dbOperation;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DbUtil {
	public static Connection con;
	public static Statement stmt;
	public static ResultSet rs;

	public static void createDbConnection() {

		try {
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/hr_management_system", "root", "@Sagar05");
			stmt = con.createStatement();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public static void executeQuery(String query) {
		try {
			stmt.execute(query);

		} catch (SQLException e) {
			e.printStackTrace();

		}
	}
	public static ResultSet executeQueryGetResult(String query) {
		ResultSet resultset = null;
		try {
			resultset = stmt.executeQuery(query);
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return resultset;
	}
	public boolean checkLogin(String username, String password) {
        String query = "SELECT * FROM users WHERE username = ? AND password = ?";
        try (PreparedStatement preparedStatement = con.prepareStatement(query)) {
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);
            ResultSet resultSet = preparedStatement.executeQuery();
            return resultSet.next();
        } catch (SQLException e) {
            e.printStackTrace();
        }
		return false;
	}
}
