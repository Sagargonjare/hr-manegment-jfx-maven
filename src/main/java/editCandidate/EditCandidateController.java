package editCandidate;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import candidates.Candidates;
import dashboard.Dashboard;
import dbOperation.DbUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import recrutement.Recrutement;
import searchCandidate.SearchCandidate;
import users.User;

public class EditCandidateController implements Initializable{
	@FXML
	private TextField candidateToEdit;
	@FXML
	private ComboBox vacancy;
	@FXML
	private TextField first;
	@FXML
	private TextField last;
	@FXML
	private TextField email;
	@FXML
	private TextField number;
	@FXML
	private TextField keyword;
	@FXML
	private TextField notes;
	@FXML
	private Button save;
	@FXML
	private Button search;
	@FXML
	private Button admin;
	@FXML
	private Button recruitment;
	@FXML
	private Button dashboard;
	@FXML
	private Button back;
	@FXML
	void Select(ActionEvent event) {
		String s = vacancy.getSelectionModel().getSelectedItem().toString();

	}
	

	@Override
	public void initialize(URL url, ResourceBundle rb) {
		ObservableList<String> list = FXCollections.observableArrayList("Associate IT Manager", "Junior Account Assistant", "Software Engineer");
		vacancy.setItems(list);
	}
	public void back(ActionEvent event) {
		new Candidates().show();
	}
	public void admin(ActionEvent event) {
		new User().show();
	}
	public void search(ActionEvent event) {
		new User().show();
	}

	public void recruitment(ActionEvent event) {
		new Recrutement().show();

	}

	public void dashboard(ActionEvent event) {
		new Dashboard().show();

	}

	public void save(ActionEvent event) throws SQLException {
		System.out.println(vacancy.getValue());
		System.out.println(first.getText());
		System.out.println(last.getText());
		System.out.println(email.getText());
		System.out.println(number.getText());
		System.out.println(keyword.getText());
		System.out.println(notes.getText());
		  if (first.getText().isEmpty() ||  last.getText().isEmpty() || email.getText().isEmpty() || number.getText().isEmpty() || keyword.getText().isEmpty()||notes.getText().isEmpty() ||  candidateToEdit.getText().isEmpty()) {
              Alert alert = new Alert(Alert.AlertType.ERROR);
              alert.setTitle("Error");
              alert.setHeaderText(null);
              alert.setContentText("Please fill in all fields.");
              alert.showAndWait();
              return;
          }

		  else {
		  String query1="select count(*) from candidate where first_name='" +candidateToEdit.getText() + "';";
		  ResultSet rs1=DbUtil.executeQueryGetResult(query1);
		  rs1.next();
		  if(rs1.getInt(1)==1) {


     
		String query = "update candidate set first_name ='" +first.getText() + "',last_name"
				+ " ='" + last.getText() + "',vacancy ='"
		+ vacancy.getValue() + "',email ='" + email.getText() + "',mobile_number ='" + number.getText() +
		"',keyword ='" + keyword.getText() + "',notes ='" + notes.getText() + "' where first_name='"
		+ candidateToEdit.getText() + "';";
		//ResultSet resultSet = DbUtil.executeQueryGetResult(query);
		System.out.println(query);
		DbUtil.executeQuery(query);
		System.out.println("Event occur admin controller " + event.getEventType().getName());

		new SearchCandidate().show();
		  }else {
			  Alert alert = new Alert(Alert.AlertType.ERROR);
              alert.setTitle("Error");
              alert.setHeaderText(null);
              alert.setContentText("Check Candidate Name is present already or not");
              alert.showAndWait();
              return;

		  }
		  }
	}
}
