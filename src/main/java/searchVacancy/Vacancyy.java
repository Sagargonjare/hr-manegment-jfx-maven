package searchVacancy;

import javafx.beans.property.SimpleStringProperty;

public class Vacancyy {
	public SimpleStringProperty jobtitle=new SimpleStringProperty();
	public SimpleStringProperty vacancy=new SimpleStringProperty();

	public SimpleStringProperty status=new SimpleStringProperty();

	public SimpleStringProperty manager=new SimpleStringProperty();

	  public String getUserRole(){
	       return jobtitle.get();
	   }

	   public String getEmployeeName(){
	       return vacancy.get();
	   }

	   public String getStatus(){
	       return status.get();
	   }

	   public String getUserName(){
	       return manager.get();
	   }
}
